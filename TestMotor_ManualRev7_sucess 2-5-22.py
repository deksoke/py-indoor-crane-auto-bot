from turtle import delay
import RPi.GPIO as GPIO
import sys, tty, termios, time
import curses
import socket
import struct



#Motor Right
BackwardRight = 5 #yellow line
ForwardRight = 13 #green line

#Motor Left
BackwardLeft = 23 #yellow line
ForwardLeft = 18 #green line


G = 16

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
#GPIO.setup(ENA, GPIO.OUT)
GPIO.setup(5, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)
#GPIO.setup(ENA0, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(9, GPIO.OUT) #Manual
GPIO.setup(11, GPIO.OUT) #Auto
GPIO.setup(20, GPIO.OUT) #red led
GPIO.setup(16, GPIO.OUT) #yellow led
GPIO.setup(12, GPIO.OUT) #green led
GPIO.setup(10,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(22,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(19,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(26,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(9,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(11,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(4,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.output(18, GPIO.HIGH)
GPIO.output(23, GPIO.HIGH)   
GPIO.output(5, GPIO.HIGH)
GPIO.output(13, GPIO.HIGH)

n = 0
s = 0
i = 0
loop = 0
Rx_bot_real = 0

sys.path.append("..")
array_ch = []
UDP_IP_ADDRESS_Jetson = "192.168.0.144"   #ip address Jetson
UDP_PORT_NO_Jetson_Receive = 8888        #Port Jetson Receive data 

try:
    def resetParameter():
        i = 0
        loop = 0

    while True:
        
        
        if GPIO.input(9) == 1: #Switch  Manual
            if  GPIO.input(22) == 1:
    
                GPIO.output(16, GPIO.LOW) #Yellow led state 
                GPIO.output(12, GPIO.HIGH) #Green led state
                GPIO.output(20, GPIO.LOW) #Red led state
        
                GPIO.output(18, GPIO.HIGH)
                GPIO.output(5, GPIO.LOW)
                #pwm.ChangeDutyCycle(100)
        
                GPIO.output(13, GPIO.HIGH)
                GPIO.output(23, GPIO.LOW)
                #pwm1.ChangeDutyCycle(100)
                print("Backward")
        
            if  GPIO.input(10) == 1:
        
                GPIO.output(16, GPIO.LOW) #Yellow led state 
                GPIO.output(12, GPIO.HIGH) #Green led state
                GPIO.output(20, GPIO.LOW) #Red led state

                GPIO.output(18, GPIO.LOW)
                GPIO.output(5, GPIO.HIGH)
                #pwm.ChangeDutyCycle(60)
        
                GPIO.output(13, GPIO.LOW)
                GPIO.output(23, GPIO.HIGH)
                #pwm1.ChangeDutyCycle(100)
                print("Forward")
            
        
        
            if GPIO.input(10) == 0 and GPIO.input(22) == 0:
                GPIO.output(16, GPIO.HIGH) #Yellow led state 
                GPIO.output(12, GPIO.LOW) #Green led state
                GPIO.output(20, GPIO.LOW) #Red led state
        
                GPIO.output(18, GPIO.HIGH)
                GPIO.output(23, GPIO.HIGH)
                #pwm.ChangeDutyCycle(0)
        
                GPIO.output(5, GPIO.HIGH)
                GPIO.output(13, GPIO.HIGH)
                #pwm1.ChangeDutyCycle(0)
                time.sleep(1)
                GPIO.output(16, GPIO.LOW) #Yellow led state  
                GPIO.output(12, GPIO.LOW) #Green led state
                GPIO.output(20, GPIO.LOW) #Red led state
                print("STOP")
            
    
            
        if GPIO.input(11) == 1: #Switch Auto
            print("AUTO")
            print("Ready")
            time.sleep(5)
            i = 0
            loop = 0 
            
           # while s == 0 :
            while i < 5:

                GPIO.output(16, GPIO.LOW) #Yellow led state 
                GPIO.output(12, GPIO.HIGH) #Green led state
                GPIO.output(20, GPIO.LOW) #Red led state

                if GPIO.input(9) == 1 :#and GPIO.input(11) == 0:
                    i = 10
                    loop = 10
                    GPIO.output(16, GPIO.HIGH) #Yellow led state 
                    GPIO.output(12, GPIO.LOW) #Green led state
                    GPIO.output(20, GPIO.LOW) #Red led state
            
                    GPIO.output(18, GPIO.HIGH)
                    GPIO.output(23, GPIO.HIGH)
                    #pwm.ChangeDutyCycle(0)
            
                    GPIO.output(5, GPIO.HIGH)
                    GPIO.output(13, GPIO.HIGH)
                    #pwm1.ChangeDutyCycle(0)
                    time.sleep(1)
                    GPIO.output(16, GPIO.LOW) #Yellow led state 
                    GPIO.output(12, GPIO.LOW) #Green led state
                    GPIO.output(20, GPIO.LOW) #Red led state
                    print("STOP 1")  

                while loop == 2 and i == 2:

                    GPIO.output(18, GPIO.LOW) #Motor ForwardLeft, HIGH = 0
                    GPIO.output(5, GPIO.HIGH) #Motor BackwardRight, HIGH = 0
                    #pwm.ChangeDutyCycle(60)
                    GPIO.output(13, GPIO.LOW) #Motor ForwardRight, HIGH = 0
                    GPIO.output(23, GPIO.HIGH) #Motor BackwardLeft, HIGH = 0
                    #pwm1.ChangeDutyCycle(100)
                    print("Motor Forward 3 ")
                    
                    i = 0
                    loop = 0
                    n += 1
                    print("n = ", n)
                    if GPIO.input(9) == 1 :
                        GPIO.output(18, GPIO.HIGH) #Motor ForwardLeft, HIGH = 0
                        GPIO.output(5, GPIO.HIGH) #Motor BackwardRight, HIGH = 0
                        #pwm.ChangeDutyCycle(60)           
                        GPIO.output(13, GPIO.HIGH) #Motor ForwardRight, HIGH = 0
                        GPIO.output(23, GPIO.HIGH) #Motor BackwardLeft, HIGH = 0
                        #pwm1.ChangeDutyCycle(100)
                        i = 10
                        loop = 10
                        #resetParameter()

                    elif GPIO.input(9) == 1 :
                        GPIO.output(18, GPIO.HIGH) #Motor ForwardLeft, HIGH = 0
                        GPIO.output(5, GPIO.HIGH) #Motor BackwardRight, HIGH = 0
                        #pwm.ChangeDutyCycle(60)           
                        GPIO.output(13, GPIO.HIGH) #Motor ForwardRight, HIGH = 0
                        GPIO.output(23, GPIO.HIGH) #Motor BackwardLeft, HIGH = 0
                        #pwm1.ChangeDutyCycle(100)
                        i = 10
                        loop = 10
                        #resetParameter()
                    
                        
                while loop == 1 and GPIO.input(26) == 1 and GPIO.input(4) == 1 :
                    
                    GPIO.output(18, GPIO.LOW) #Motor ForwardLeft, HIGH = 0
                    GPIO.output(5, GPIO.HIGH) #Motor BackwardRight, HIGH = 0
                    #pwm.ChangeDutyCycle(60)
                    GPIO.output(13, GPIO.LOW) #Motor ForwardRight, HIGH = 0
                    GPIO.output(23, GPIO.HIGH) #Motor BackwardLeft, HIGH = 0
                    #pwm1.ChangeDutyCycle(100)
                    print("Motor Forward 2 ")
                    time.sleep(2)
                    i = 2
                    loop = 2
                    print("Step 2 =",i)
                    print("loop 2 =",loop)
                    
                    if GPIO.input(9) == 1 :
                        GPIO.output(18, GPIO.HIGH) #Motor ForwardLeft, HIGH = 0
                        GPIO.output(5, GPIO.HIGH) #Motor BackwardRight, HIGH = 0
                        #pwm.ChangeDutyCycle(60)           
                        GPIO.output(13, GPIO.HIGH) #Motor ForwardRight, HIGH = 0
                        GPIO.output(23, GPIO.HIGH) #Motor BackwardLeft, HIGH = 0
                        #pwm1.ChangeDutyCycle(100)
                        i = 10
                        loop = 10
                        #resetParameter()


                while loop == 0 :
                    GPIO.output(18, GPIO.LOW) #Motor ForwardLeft, HIGH = 0
                    GPIO.output(5, GPIO.HIGH) #Motor BackwardRight, HIGH = 0
                    #pwm.ChangeDutyCycle(60)
                    GPIO.output(13, GPIO.LOW) #Motor ForwardRight, HIGH = 0
                    GPIO.output(23, GPIO.HIGH) #Motor BackwardLeft, HIGH = 0
                    #pwm1.ChangeDutyCycle(100)
                    print("Motor Forward 1 ")
                    print("n = ", n)
                                        
                    if GPIO.input(26) == 1 :
                        GPIO.output(18, GPIO.HIGH) #Motor ForwardLeft, HIGH = 0
                        GPIO.output(5, GPIO.HIGH) #Motor BackwardRight, HIGH = 0
                        #pwm.ChangeDutyCycle(60)           
                        GPIO.output(13, GPIO.HIGH) #Motor ForwardRight, HIGH = 0
                        GPIO.output(23, GPIO.HIGH) #Motor BackwardLeft, HIGH = 0
                        #pwm1.ChangeDutyCycle(100)
                        print("Motor Pause")
                        print("Sensor Tick")
                        #print("Step 1 =",i)
                        #print("loop 1 =",loop)
                        time.sleep(0.5)
                        i = 1
                        loop = 1


                    #elif GPIO.input(9) == 0 and s == 1 and GPIO.input(26) == 1:
                        #i = 1
                        #loop = 1





 
                       

    
        
                        
        
finally:

    GPIO.cleanup()